resource "aws_s3_bucket" "terraformdata" {
  bucket = "alehbayarevich.art"
  acl = "private"

  versioning {
      enabled = true
  } 
}